Copy these files to your web server.

Copy 'config.template.php' to 'config.php' and personalize some parameters.

'setUserPosition.php' will allow communicating positions with the server.
'index.php' shows the current position.
'index_openlayer.php' does not use 'Google Maps' and hence reverts to the use of openlayers
'viewer.php' allows you to see the history on a map.
'checker.php' can help debug some stuff.
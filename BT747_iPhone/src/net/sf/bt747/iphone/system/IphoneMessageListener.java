/**
 * 
 */
package net.sf.bt747.iphone.system;

/**
 * @author Mario De Weerd
 * 
 */
public interface IphoneMessageListener {
    void postMessage(final String message);
}

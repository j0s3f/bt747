/*
 * ufolib - Full unicode font range for Superwaba
 * Copyright (C) 2003 Oliver Erdmann
 * http://jdict.sf.net 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License Version 2.1 as published by the Free Software Foundation
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

package ufolib.engine;


public class UfoCopyright {

    public static String copyrightNL = 
	"This software: Copyright 2003 Oliver Erdmann,\nsee also: http://jdict.sf.net/ufolib\n" +
	"\n" +
	"Parts of the software: Copyright 2000-2003\nGuilherme Campos Hazan <guich@superwaba.org.>\nhttp://www.superwaba.org\n" +
	"\n" + 
	"Parts of the software: Copyright by the\nApache Software Foundation (http://www.apache.org/)\n" +
	"\n" +
	"Parts of the software are made with\nSun One Studio 4, (C) Sun Microsystems Inc.\n" +
	"\n" +
	"Parts of the software: Copyright 1996-1999\nby Scott Hudson, Frank Flannery, C. Scott Ananian\n" +
	"\n" +
	"Parts of the software: Copyright 1996-2003\nby Elliot Joel Berk and C. Scott Ananian\n" +
	"\n" + 
	"Parts of the software: Copyright 1998-2002\nAaron M. Renn (arenn@urbanophile.com)\n" +
	"\n" +
	"Parts of the software: Copyright 1994-2003\nThe XFree86 Project, Inc. All Rights Reserved.\n" +
	"\n" +
	"This library is free software; you can redistribute it and/or\n" +
	"modify it under the terms of the GNU Lesser General Public\n" +
	"License Version 2.1 as published by the Free Software Foundation\n" +
	"This library is distributed in the hope that it will be useful,\n" +
	"but WITHOUT ANY WARRANTY; without even the implied warranty of\n" +
	"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU\n" +
	"Lesser General Public License for more details.\n" +
	"You should have received a copy of the GNU Lesser General Public\n" +
	"License along with this library; if not, write to the Free Software\n" +
	"Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA\n" +
	"\n";
}

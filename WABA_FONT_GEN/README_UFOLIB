ufolib README
http://jdict.sf.net/ufolib

 
RUN DEMO
--------
- To run the Font Info Application, run runFontInfo.sh
- To run one demo, run runDemo1.sh
- To run another demo, run runDemo2.sh

- On PalmOS device you need
  - the program files:
      UfoFontInfo.pdb
      UfoFontInfo.prc
      UfoDemo1.pdb
      UfoDemo1.prc
      UfoDemo2.pdb
      UfoDemo2.prc
  - the fonts:
      UFFgenXX12n.pdb
      UFFgenXX17n.pdb
      UFFgnui1k16n.pdb
  - superwaba, if not present:
      SWNatives.prc
      SuperWaba.pdb
      SuperWaba.prc

The SuperWaba files can be found in the directory
called "superwaba"; more about SuperWaba here:
http://www.superwaba.org/

There are runFontInfoInPose.sh, runDemo1InPose.sh and 
runDemo2InPose.sh to run them in POSE



USE IN YOUR SUPERWABA PROGRAMS
------------------------------
Its easy. You need the classes in the package ufolib.engine.*,
using either ufolibengine.jar or ufolib.jar. Then

  UfoLabel l = new UfoLabel( "Hello Ufolib" ); 

that's it.

You will find more examples in UfoDemo1.java and the javadocs,
which are placed in the directory called "javadocs".



RUN FONTIZER (GUI)
------------------
Run the files runFontizer.sh or runFontizer.bat to
convert font files.


RUN FONTIZER (COMMAND LINE INTERFACE)
-------------------------------------
Run the file runCmdFontizer.sh. This is the usage info of Fontizer
  -i          start interactive gui (this is the default), vs. -c/-b 
  -c <file>   start command line interface, vs. -i/-b
  -b <file>   start batch job, commands given in <file>, vs. -i/-c
  -f <file>   load this source font on startup
  -p <file>   load this profile on startup
  -u <fam>    use this as UFF family name
  -v <size>   use this as UFF pixelsize
  -w <nbi>    use this as UFF shape
  -x          save UFF and exit, use with -f -p -u -v -w 
  -q          be quiet


RUN BDF CONVERTER
-----------------
See the files runBDFConverter.sh or runBDFConverter.bat how to run the
converter. Replace the BDF font file name "UX.bdf" with your
font. This is the usage info:

-n <name>   the fonts short name, no spaces, eg. "gnuuni" 
-s [nbi]    the slant: n normal, b bold, i italic, bi bold italic
-p <size>   the point size, eg. 12
-f <file>   the PDB filename
-l <pixel>  (extra) leading
-w <pixel>  extra (white)space between chars
-v          verbose output

argument(s): <bdf-filename>



FOR DEVELOPERS to look at the sources
-------------------------------------
ufolib is found in CVS, here:
http://cvs.sourceforge.net/cgi-bin/viewcvs.cgi/jdict/jdict/ufolib/
you can browse the current snapshot.

or get it:
  cvs -d:pserver:anonymous@cvs.sourceforge.net:/cvsroot/jdict login
    <password is simply return>
  cvs -z3 -d:pserver:anonymous@cvs.sourceforge.net:/cvsroot/jdict co jdict

or:
  export CVSROOT=:pserver:anonymous@cvs.jdict.sourceforge.net:/cvsroot/jdict
  cvs login
  cvs co jdict

1. Copy and edit the file ant-localprop

2. Compile it
     ant c

3. Run applications:

   3.1 Run the converter application
          ant f
       and create a font PDB.

   3.2 Get an BDF file, remove the chars you do not want, 
       name it "x.bdf" and run the converter 
          ant r
       This produces the PDB.

   3.3 Run the Font Info Application:
          ant ri

   3.4 Run the demos:
          ant rd
       or
          ant rd2

   3.4 Run the eval app:
          ant re

4. You can warp&exegen the Font Info Application:
     ant wei

5. Then you can run the Font Info Application in POSE:
     ant pi

6. You can warp&exegen the demos:
     ant wed
   or
     ant wed2

7. Then you can run the demos in POSE:
     ant pd
   or 
     ant pd2


---------------

This software: 
Copyright 2003 Oliver Erdmann,
see also: http://jdict.sf.net/ufolib

Parts of the software: Copyright 2000-2003
Guilherme Campos Hazan <guich@superwaba.org.>
http://www.superwaba.org
 
Parts of the software: Copyright by the
Apache Software Foundation (http://www.apache.org/)

Parts of the software are made with
Sun One Studio 4, (C) Sun Microsystems Inc.

Parts of the software: Copyright 1996-1999
by Scott Hudson, Frank Flannery, C. Scott Ananian

Parts of the software: Copyright 1996-2003
by Elliot Joel Berk and C. Scott Ananian
 
Parts of the software: Copyright 1998-2002
Aaron M. Renn (arenn@urbanophile.com)

Parts of the software: Copyright 1994-2003
The XFree86 Project, Inc. All Rights Reserved.

Parts are:
Copyright (C) 2000 by George Williams
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
Redistributions of source code must retain the above copyright notice, this
list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice,
this list of conditions and the following disclaimer in the documentation
and/or other materials provided with the distribution.
The name of the author may not be used to endorse or promote products
derived from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

------------------------------

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License Version 2.1 as published by the Free Software Foundation
This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public
License along with this library; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA




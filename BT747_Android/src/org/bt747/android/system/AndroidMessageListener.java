/**
 * 
 */
package org.bt747.android.system;

/**
 * @author Mario De Weerd
 * 
 */
public interface AndroidMessageListener {
    void postMessage(final String message);
}
